# API Rest for testing
Esta es una API Rest creada para realizar pruebas de aplicaciones.
Contando con las funciones básicas (CRUD) para el manejo de la tabla tareas (tasks).
*Desarrollada con **node.js + express + lowdb**.*
*Deploy: Zeit Now v2*

- **URL BASE**
    * https://lowdb-restapi-ozhik9m1w.now.sh

Para utilizar el servicio agregar al URL BASE algún endpoint de los mencionados en la parte posterior.

- **Métodos**
	- 
	* **Obtener tareas**
		* Request type: **`GET`**
		* Endpoint: **`/api/tasks`** 
		* Data Params: -
		* Success Response: 
			* Code: `200`
			   Content: 
			    ```json  
			    [
				   { 
				     "id":  "893e6c01-a937-446b-b6a8-95a640e21478",
		    		 "name":  "Tarea 1",
		  			 "description":  "Primer tarea del día"		
		  		   }
		  		   .
		  		   .
		  		   .
		  		]
	  		  ```
	  		  
		* Error Response: 
			* Code: `400 BAD REQUEST`
			  Content:  `{ error : "Inválido" }`
		* Sample Call:
			```javascript
		    $.ajax({
		        url: "https://lowdb-restapi-ozhik9m1w.now.sh/api/tasks",
		        dataType: "json",
		        type : "GET",
		        success : function(r) {
		          console.log(r);
  		      }
		    });
		  ```
			
	* **Obtener tarea específica**
		* Request type: **`GET`**
		* Endpoint: **`/api/tasks/:id`** 
		* Data Params: 
			* Required: 
			`id=[string]`
		* Success Response: 
			* Code: `200` 
			    Content: 
			   ```json  
			   { 
			     "id":  "1498bb2d-496d-432b-9af8-cf8ace7ab032",
	    		 "name":  "Tarea 2",
	  			 "description":  "Segunda tarea del día"		
	  		  }
	  		  ```
			   
		* Error Response: 
			* Code: `400 BAD REQUEST`
			  Content:  `{ error : "Inválido" }`
	  * Sample Call:
		  ```javascript
		    $.ajax({
		        url: "https://lowdb-restapi-ozhik9m1w.now.sh/api/tasks/0db34db4-366c-454f-9b6b-c91e11ce85a2",
		        dataType: "json",
		        type : "GET",
		        success : function(r) {
		          console.log(r);
  		      }
		    });
		  ```

	* **Crear tarea**
		* Request type: **`POST`**
		* Endpoint: **`/api/tasks`** 
		* Data Params: 
			* Body:
				```json 
				   { 
		    		 "name":  "Tarea 1",
		  			 "description":  "Primer tarea del día"		
		  		   }
		  		 ```
	   
		* Success Response: 
			* Code: `200`
			   Content: 
			   ```json  
			   { 
			     "id":  "1498bb2d-496d-432b-9af8-cf8ace7ab032",
	    		 "name":  "Tarea 1",
	  			 "description":  "Primer tarea del día"		
	  		  }
	  		  ```
		* Error Response: 
			* Code: `400 BAD REQUEST`
			  Content:  `{ error : "Inválido" }`
		 * Sample Call:
			```javascript
		    $.ajax({
		        url: "https://lowdb-restapi-ozhik9m1w.now.sh/api/tasks",
		        dataType: "json",
		        type : "POST",
		        data : { "name":"Tarea 1","description":"Primer tarea del día" }
		        success : function(r) {
		          console.log(r);
  		      }
		    });
			```
		  
	* **Modificar tarea**
		* Request type: **`PUT`**
		* Endpoint: **`/api/tasks/:id`** 
		* Data Params: 
			* Required: 
			`id=[string]`
			
			* Body:
				```json 
				   { 
		    		 "name":  "Tarea 1",
		  			 "description":  "Primer tarea del día COMPLETADA"		
		  		   }
		  		 ```
		* Success Response: 
			* Code: `200`
			   Content: 
			   ```json  
			   { 
			     "id":  "1498bb2d-496d-432b-9af8-cf8ace7ab032",
	    		 "name":  "Tarea 1",
				 "description":  "Primer tarea del día COMPLETADA"
	  		  }
	  		  ```
		* Error Response: 
			* Code: `400 BAD REQUEST`
			  Content:  `{ error : "Inválido" }`
			  
		 * Sample Call:
			```javascript
		    $.ajax({
		        url: "https://lowdb-restapi-ozhik9m1w.now.sh/api/tasks/1498bb2d-496d-432b-9af8-cf8ace7ab032",
		        dataType: "json",
		        type : "PUT",
		        data : { "name":"Tarea 1","description":"Primer tarea del día COMPLETADA" }
		        success : function(r) {
		          console.log(r);
  		      }
		    });
			```
			 
	* **Eliminar tarea**
		* Request type: **`DELETE`**
		* Endpoint: **`/api/tasks/:id`** 
		* Data Params: 
			* Required: 
			`id=[string]`
			
		* Success Response: 
			* Code: 200  
			   Content: `{}`
		* Error Response: 
			* Code: `400 BAD REQUEST`  
			  Content:  `{ error : "Inválido" }`
		 * Sample Call:
			 ```javascript
		    $.ajax({
		        url: "https://lowdb-restapi-ozhik9m1w.now.sh/api/tasks/1498bb2d-496d-432b-9af8-cf8ace7ab032",
		        dataType: "json",
		        type : "DELETE",
		        success : function(r) {
		          console.log(r);
  		      }
		    });
			```