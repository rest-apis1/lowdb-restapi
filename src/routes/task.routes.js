const { Router } = require('express')
const router = Router();

const { getTasks, createTask, getTask, updateTask, deleteTask } = require('../controllers/task.controller')

router.get('/api/tasks',getTasks);
router.get('/api/tasks/:id',getTask);
router.post('/api/tasks',createTask);
router.put('/api/tasks/:id',updateTask);
router.delete('/api/tasks/:id',deleteTask);

module.exports = router;