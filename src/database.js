// Archivo de configuracion para lowDb
const low = require('lowdb');
const FileAsync = require('lowdb/adapters/FileAsync');

let db;
var fs = require('fs');


// ================ PARA EL DEPLOY EN ZEIT NOW - PRODUCCIÓN // ================
function createConnection(){

    // CREANDO EL ARCHIVO db.json EN CARPETA tmp (TEMPORAL) -> NECESARIO PARA DEPLOY EN PROD ZEIT NOW
    var dataInicial = {tasks: []}
    var dInicial = JSON.stringify(dataInicial);

    fs.writeFile('/tmp/db.json', dInicial, function(err){
        if(err) return console.error(err);
        // FUNCIÓN PARA LEER ARCHIVO TEMPORAL | PARA LOCAL NO ES NECESARIO LEER EL TEMPORAL
        createDatabase();
    });

}
async function createDatabase(){
    const adapter = new FileAsync('/tmp/db.json');
    db = await low(adapter);
    db.defaults({tasks: []}).write();
}
// ================ [FIN] - PARA EL DEPLOY EN ZEIT NOW - PRODUCCIÓN // ================

/* PARA DEPLOY LOCAL - ZEIT NOW DEV
*

async function createConnection(){

    const adapter = new FileAsync('db.json');
    db = await low(adapter);
    db.defaults({tasks: []}).write();
}

*
*/

const getConnection = () => db;

module.exports = {
    createConnection,
    getConnection
}